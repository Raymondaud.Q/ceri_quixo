// RAYMONDAUD Quentin
// Simple POC webserver

const http = require('http');
const express = require('express')
const app = express()
const port = process.env.PORT || 3019;
app.use(express.static(__dirname + '/client/'));
app.get('/', (req, res) => {
  res.sendFile('index.html', {root: __dirname })
})

// CREATE HTTP SERVER
const server = http.createServer(app);
server.listen(port, () => {
  console.log(`Quixo-APP on http://localhost:${port}`)
})

// CREATE WEBSOCKET SERVER
const Socket = require('./socketServer');
serverSocket = Socket(server);