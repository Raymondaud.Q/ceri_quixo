// RAYMONDAUD Quentin
// Socket server that share a single quixo game with all clients


const WebSocket = require('ws'); // new
const Quixo = require("./QuixoCore.js")


function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

// WebSocket broadcast function
function broadcast(server,data) {
    server.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN)
            client.send(data);
    });
}

// Exportable serverSocket module
module.exports = function serverSocket(server){
    let quixo = new Quixo();
    const serverSocket = new WebSocket.Server({server:server});
    serverSocket.on('connection', (socketClient) => {
        // When a new client is connected, send him the board
        socketClient.send( "[" + JSON.stringify(quixo.getBoard()) + ", " + quixo.getRound() + "]");
        // Client request handling
        socketClient.on('message', function incoming(message) {
            if ( isJson(message)){
                let array = JSON.parse(message)
                if ( array[0] ){            // If array[0] exists it means that a player has sent a move
                    quixo.play(array[0].row, array[0].column, array[1].row, array[1].column) 
                    broadcast(serverSocket,  "[" + JSON.stringify(quixo.getBoard()) + ", " + quixo.getRound() + "]" );
                    console.log(quixo.toString()+"\n");
                    if ( quixo.isFinished() )
                        quixo = new Quixo()
                }
            }
            else // if WTF is received just show it  
                console.log(message)

        });
        // If no client connected to the game, just reset the game
        socketClient.on('close', (socketClient) => {
            if ( serverSocket.clients.size === 0 ){
                console.log("No clients, reinit quixo")
                quixo = new Quixo();
            }
        });

    });
    return serverSocket
}

