# JavaScript Quixo Game

[**Here you can play Quixo-Game**](https://quentinraymondaud.itch.io/quixo-game)

## Dependencies

- [P5js](https://p5js.org/)
- [ExpressJs](https://expressjs.com/)
- [NodeJS & NPM](https://nodejs.org/en/download/package-manager/)
- [WS WebSocket](https://github.com/websockets/ws)

## Dependencies that you must install

- [NodeJS & NPM](https://nodejs.org/en/download/package-manager/)

## Build & Run the projet :

- Download this rep
- Extract Files
- Fill IPORT  in "/client/main.js" with the server address 
- Run `npm i`
- Run `node server.js`
- Go to localhost:3019

## Deploy the projet :

- If you had planned to deploy it on a server you must do :

	- Replace ```<script src="p5.js"></script>``` by ```<script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/1.1.9/p5.min.js"></script>``` in ./client/index.html   

	**OR**   
	
	- Open the port in order to deliver the static file : ```sudo iptables -A INPUT -p tcp --dport 3019 -j ACCEPT```
