// RAYMONDAUD Quentin
// Animated board cubes for end game

class LooserCubeVertical extends Cube {


	_shiftY;
	constructor(upperLeftY, upperLeftX, verticeLength, way) {
		super(upperLeftY, upperLeftX, verticeLength);
		this._shiftY = 0;
		this._way = way;
    }

    show() {
	
		if ( this._state === CubeStates.FREE)
			fill(255);
		else if ( this._state === CubeStates.CROSS )
			fill(255,0,0);
		else
			fill(0,0,255);
		
		if ( this._way )
			this._shiftY -= Math.random()*10;
		else
			this._shiftY += Math.random()*10;

		rect(this._ulX, this._ulY + this._shiftY , this._length, this._length);
		fill(255);
	}
}

class LooserCubeHorizontal extends Cube {


	_shiftX;
	constructor(upperLeftY, upperLeftX, verticeLength, way) {
		super(upperLeftY, upperLeftX, verticeLength);
		this._shiftX = 0;
		this._way = way
    }

    show() {
	
		if ( this._state === CubeStates.FREE)
			fill(255);
		else if ( this._state === CubeStates.CROSS )
			fill(255,0,0);
		else
			fill(0,0,255);
			
		if ( this._way )
			this._shiftX += Math.random()*10;
		else
			this._shiftX -= Math.random()*10;

		rect(this._ulX + this._shiftX, this._ulY  , this._length, this._length);
		fill(255);
	}
}