// RAYMONDAUD Quentin
// Simple animated cube

class WinnerCube extends Cube {

	_sizeReduce;
	_isReduced;
	constructor(upperLeftY, upperLeftX, verticeLength) {
		super(upperLeftY, upperLeftX, verticeLength);
		this._sizeReduce = 0;
		this._isReduced = false;
    }

    show() {
	
		if ( this._state === CubeStates.FREE)
			fill(255);
		else if ( this._state === CubeStates.CROSS )
			fill(255,0,0);
		else
			fill(0,0,255);
			
		if ( this._sizeReduce <= this._length && ! this._isReduced ){
			this._sizeReduce += 1;
			if ( this._sizeReduce >= this._length )
				this._isReduced = true;
		}
		else if ( this._isReduced ){
			this._sizeReduce -=1;
			if ( this._sizeReduce <= 0 )
				this._isReduced = false;
		}

		rect(this._ulX, this._ulY, this._length - this._sizeReduce/2, this._length - this._sizeReduce/2);
		fill(255);
	}
}