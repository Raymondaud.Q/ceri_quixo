// RAYMONDAUD Quentin
// UI cube class 
// Board elements 

class Cube {
    _ulX;
    _ulY;
    _urX; 
    _llY;
    _length;
    /* ul__length__ur
     * |           |
     * |           |
     * |           |
     * ll__________lr
     */
    constructor(upperLeftY, upperLeftX, verticeLength) {
        this.resize(upperLeftY, upperLeftX, verticeLength)
        this._state = CubeStates.FREE;
    }

    // Compute square vertices and length
    // Called by controller on WindowResize P5js trigger and in the constructor
    resize(upperLeftY, upperLeftX, verticeLength) {
        this._length = verticeLength;
        this._ulX = upperLeftX;
        this._ulY = upperLeftY;
        this._urX = this._ulX + this._length;
        this._llY = this._ulY  - this._length;
    }
        
    // Set the state ( FREE, CROSS or CIRCLE)
    // Reinit UI sugar attributes 
    setState(state){
        this._state = state;
        this._isAValidPick = 0;
        this._isAValidMove = 0;
    }

    getPosition(){
        return {x: this._ulX + this._length/2, y: this._ulY + this._length / 2};
    }

    // Create an animated looser cube with data of this instance
    convertToLooserCube(choice){
        let cube;
        if (choice === 0)
            cube = new LooserCubeVertical(this._ulY, this._ulX, this._length, true);
        else if ( choice === 1 )
            cube = new LooserCubeVertical(this._ulY, this._ulX, this._length, false);
        else if (choice === 2)
            cube = new LooserCubeHorizontal(this._ulY, this._ulX, this._length, true);
        else
            cube = new LooserCubeHorizontal(this._ulY, this._ulX, this._length, false);
        cube.setState(this._state);
        return cube
    }

    // Create an animated winner cube with data of this instance
    convertToWinnerCube(){
        let cubee = new WinnerCube(this._ulY, this._ulX, this._length);
        cubee.setState(this._state);
        return cubee
    }

    // Convert any subclass of cube into orignal cube
    convertToCube(){
        let cubee = new Cube(this._ulY, this._ulX, this._length);
        cubee.setState(this._state);
        return cubee
    }
    
    // If != 0 displays a mini-cube
    // Signals that u can pick this Cube
    setValidPick(state){
        this._isAValidPick = state;
    }
    
    // If != 0 displays a circle
    // Signals the place where u can put your current Cube
    setValidMove(state){
        this._isAValidMove = state;
    }
    
    // If true reduce the current Cube sire
    // Signals the last selectable Cube for a move or a pick
    setHovered(bol = true){
        this._hovered = bol;
    }

    isPointInCube(pointX, pointY) {
        if (pointX > this._ulX &&
                pointX < this._urX &&
                pointY-this._length < this._ulY && 
                pointY-this._length > this._llY )
            //Why the fuck -this._length ? IDK
            return true;
        return false;
    }

    // Display function called from /quixo-gui/Board.js
    // Feeds the rendering pipeline
    show() {
        if ( this._state === CubeStates.FREE)
            fill(255);
        else if ( this._state === CubeStates.CROSS )
            fill(255,0,0);
        else
            fill(0,0,255);
        
        if ( this._hovered ){
            rectMode(CENTER)
            rect(this._ulX+this._length/2, this._ulY+this._length/2, this._length*0.9, this._length*0.9)
            rectMode(CORNER)
        }
        else
            rect(this._ulX, this._ulY, this._length, this._length);
        
        if ( this._isAValidPick !== 0 ){
            rectMode(CENTER);
            if ( this._isAValidPick === CubeStates.CROSS )
                fill(255,0,0);
            else if ( this._isAValidPick === CubeStates.CIRCLE)
                fill(0,0,255)
            rect(this._ulX+this._length/2, this._ulY+this._length/2, this._length/4, this._length/4);
            rectMode(CORNER);
        }
        
        if ( this._isAValidMove !== 0 ){
            rectMode(CENTER);
            if ( this._isAValidMove === CubeStates.CROSS )
                fill(255,0,0);
            else if ( this._isAValidMove === CubeStates.CIRCLE)
                fill(0,0,255)
            circle(this._ulX+this._length/2+(Math.random()*4-2), this._ulY+this._length/2+(Math.random()*4-2), this._length/2);
            rectMode(CORNER);
        }
        
        fill(255);
    }
}