// RAYMONDAUD Quentin
// Simple Start Menu

// 4 Buttons for 4 game mode
// Look at Controller class properties & functions

class Menu{

    _buttonSolo;
    _buttonVersus;
    _buttonFullRobot;
    _buttonColabGame;
    _deepthInput;
    _explosion;
    _nbExplo;
    _reverse;
    _minMaxDeepth;

    _startSolo(reverse, deepth){ controller.startSolo(reverse, deepth); }

    _startVersus(){ controller.startVersus(); }

    _startRobot(deepth){ controller.startRobot(deepth); }

    _startColab(){ controller.startShared(); }

    constructor() {
        this._minMaxDeepth = 3;
        this._reverse = false;
        this._explosion = [];
        this._nbExplo = 0 ;
        this._buttonColabGame = createButton(' 🎮 Network Shared 🎮 ');
        this._buttonSolo = createButton(' 🎮 Single Player 🤖');
        this._buttonVersus  =  createButton(' 🎮 Versus 🎮 ');
        this._buttonFullRobot = createButton(' 🤖 Full Robot 🤖');
        this._deepthInput = createInput('' + this._minMaxDeepth + ' = Deepth');
        this._buttonSolo.style("font-family", "Comic Sans MS");
        this._buttonSolo.style("font-size", "20px");
        this._buttonVersus.style("font-family", "Comic Sans MS");
        this._buttonVersus.style("font-size", "20px");
        this._buttonFullRobot.style("font-family", "Comic Sans MS");
        this._buttonFullRobot.style("font-size", "20px");
        this._buttonColabGame.style("font-family", "Comic Sans MS");
        this._buttonColabGame.style("font-size", "20px");
        this._buttonSolo.mousePressed(() => { this._startSolo(this._reverse,this._minMaxDeepth)});
        this._buttonVersus.mousePressed(this._startVersus);
        this._buttonFullRobot.mousePressed(() => this._startRobot(this._minMaxDeepth));
        this._buttonColabGame.mousePressed(this._startColab);
        this._deepthInput.input(() => this._inputEvent(this) );
        this._buttonSizeAndPos();
    }

    removeButtons(){
        this._buttonSolo.remove();
        this._buttonVersus.remove();
        this._buttonFullRobot.remove();
        this._buttonColabGame.remove();
        this._deepthInput.remove();
    }

    // resize listener function
    resize(){ this._buttonSizeAndPos(); }

    // P5js rendering pipeline called for showing fireworks
    show(){
        background(0,5);
        for (let i = 0; i < this._explosion.length; i++){
            this._explosion[i].updater();
            this._explosion[i].show();
        }
    }

    _buttonSizeAndPos(){
        let wSize = width/1.5;
        let hSize = height/6;
        this._buttonSolo.size( wSize, hSize);
        this._buttonVersus.size(wSize, hSize);
        this._buttonFullRobot.size(wSize, hSize);
        this._buttonColabGame.size(wSize, hSize);
        this._deepthInput.size( width/20, height/30);
        let xPos = (width - this._buttonSolo.size().width)/2;
        this._buttonSolo.position( xPos, 3*height/10 );
        this._buttonVersus.position( xPos, height/2 );
        this._buttonFullRobot.position( xPos, 7*height/10 );
        this._buttonColabGame.position( xPos, height/10 );
        this._deepthInput.position( width/20, height/2 );
    }

    _inputEvent(self) {
        var test = int(this._deepthInput.value());
        if (!isNaN(test))
            self._minMaxDeepth = test; // overwrite if ( begin with ) "Good Int Number" only
        else 
           self._minMaxDeepth = 3;
    }

    // Set reverse the team you'll play VS IA 
    // 1 click on menu + click on Solo Game =  IA playing blue ( it began the game )
    // No click = You playing blue team ( you began the game)
    explose(mouseX,mouseY){

        let isButtonsClicked = (                             
            mouseX > width / 6 &&
            mouseX < 5 * width / 6 &&
            mouseY > height / 10 &&
            mouseY < 13 * height / 15 
        );

        let isInputClicked = ( 
            mouseX > this._deepthInput.position().x && 
            mouseX < ( this._deepthInput.position().x + this._deepthInput.size().width) &&
            mouseY >  this._deepthInput.position().y &&
            mouseY < ( this._deepthInput.position().y + this._deepthInput.size().height )
        );

        if ( ! isInputClicked && ! isButtonsClicked )
            this._reverse = ! this._reverse;

        if ( this._explosion.length < 100 )
            for (let i = 0; i < 40; i++){
                this._explosion.push(new Firework(mouseX, mouseY, this._reverse));
                this._nbExplo ++ ;
            }
        else
            for (let i = 0; i < 40 ; i ++) {
                this._explosion[this._nbExplo] = new Firework(mouseX, mouseY, this._reverse);
                this._nbExplo  =  (this._nbExplo + 1) % 100;

            }           
    }
    
}

