// RAYMONDAUD Quentin

// UI Sugar 
// Showing particle when a move is played
// The particle goes from src cube (x,y) to dest cube (ex,ey)

class TraceObject{
	constructor( x, y, ex, ey, reverse){
		this.x = x;
		this.y = y;
		this._reverse = reverse;
		this.xSpeed = (ex-x) / 100;
		this.ySpeed = (ey-y) / 100;
		this.size = random(20,40);
	}
	
	updater(){
		this.x += this.xSpeed ;
		this.y += this.ySpeed ;
		this.size *= 0.97;
	}
	
	show(){
		push();
		translate(this.x, this.y);
		let randColor = random(55,155);
		let randBlanc = random(0,1);
		if ( randBlanc > 0.1 )
			if ( ! this._reverse )
				fill(100+randColor,0+randColor/2,0)
			else
				fill(0+randColor/2,0,100+randColor);
		else 
			fill(255,255,255);
		noStroke();
		rotate(frameCount);
		ellipse(0, 0, this.size * random(0.5,1.5), this.size * random(0.5,1.5));
		pop();    
	}
}