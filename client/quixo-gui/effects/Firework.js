// RAYMONDAUD Quentin
// Simple firework class

class Firework {
	constructor( _x, _y, reverse){
		this.x = _x;
		this.y = _y;
		this._reverse = reverse;
		this.xSpeed = random(-2,2);
		this.ySpeed = random(-1,1) * sqrt(4 - this.xSpeed * this.xSpeed)
		this.grav = 0.8;
		this.size = random(30,50);
	}
	
	updater(){
		this.ySpeed += 0.02 * this.grav;
		this.x += this.xSpeed * random(0, 4);
		this.y += this.ySpeed * random(0, 4);
		this.size *= 0.97;
	}
	
	// call P5js rendering pipeline
	show(){
		push();
		translate(this.x, this.y);
		let randColor = random(0,155);
		let randBlanc = random(0,1);
		if ( randBlanc > 0.1 )
			if ( this._reverse )
				fill(100+randColor,0+randColor/2,0)
			else
				fill(0+randColor/2,0,100+randColor);
		else 
			fill(255,255,255);
		noStroke();
		rotate(frameCount);
		ellipse(0, 0, this.size * random(0.5,1.5), this.size);
		pop();    
	}
}