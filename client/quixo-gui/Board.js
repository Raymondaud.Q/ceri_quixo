// RAYMONDAUD Quentin
// UI board that contains & display cubes

class Board {

    boardSize;
    cubeSize;
    _board;

    // Compute UI elements size in order to fits well the screen
    // Build UI model according to results
    constructor() {
        this._explosion = [];
        this._nbExplo = 0;

        this.boardSize = Math.min(width, height);
        this.cubeSize = this.boardSize / 6;
        this._board = [];
        this._initBoard();
        
    }

    _initBoard(){
        for (let j = 0; j < 5; j++) {
            this._board[j] = [];
            for (let i = 0; i < 5; i++) {
                this._board[j][i] = new Cube(
                    // upper left Y  
                    (height - this.boardSize) / 2 +
                    (this.boardSize - this.cubeSize) / 10 + this.cubeSize * j,
                    // upper left X
                    (width - this.boardSize) / 2 +
                    (this.boardSize - this.cubeSize) / 10 + this.cubeSize * i,
                    // cube length
                    this.cubeSize
                );
            }
        }
    }

    // Called by controller on WindowResize P5js trigger
    // Compute and resize UI model
    resize() {
        this.boardSize = Math.min(width, height);
        this.cubeSize = this.boardSize / 6;
        this._initBoard()
        
    }

    // Returns Indexes of the cube which contains the (point mouseX,mouseY)
    getCubeIndex(mouseX, mouseY) {
        for (let j = 0; j < 5; j++) {
            for (let i = 0; i < 5; i++) {
                if (this._board[i][j].isPointInCube(mouseX, mouseY)){
                    return {
                        column: j,
                        row: i
                    };
                }
            }
        }
    }
    
    // Returns UI Cube which contains the (point mouseX,mouseY)
    getCube(mouseX, mouseY) {
        for (let j = 0; j < 5; j++) {
            for (let i = 0; i < 5; i++) {
                if (this._board[i][j].isPointInCube(mouseX, mouseY)){
                    return this._board[i][j];
                }
            }
        }
    }

    // Set Cube properties according to Quixo-Engine & Controller states
    /*
        const CubeStates = Object.freeze({
            "CROSS": -1,
            "CIRCLE": 1,
            "FREE": 0
        });
        Position = { row: y, column:x }

        validPicks & validMoves = Array(Position)
        boardCp = Array(Array(CubeStates))
    */
    updateBoard(boardCp, validPicks, validMoves, cubeState) {
        for (let j = 0; j < boardCp.length; j++) {
            for (let i = 0; i < boardCp[j].length ; i++) {
                this._board[j][i].setState(boardCp[j][i]);
            }
        }
        if (validPicks) {
            let nbVP = validPicks.length;
            for (let vp = 0; vp < nbVP; vp++) {
                    this._board[validPicks[vp].row][validPicks[vp].column].setValidPick(cubeState);
            }
        }
        if (validMoves) {
            let nbVM = validMoves.length;
            for (let vm = 0; vm < nbVM; vm++) {
                this._board[validMoves[vm].row][validMoves[vm].column].setValidMove(cubeState);
            }
        }
    }

    // Convert all animated cube into original father class Cube
    reinitCubes(){
        this._winner = undefined;
        for (let j = 0; j < 5; j++)
            for (let i = 0; i < 5; i++)
                this._board[i][j] = this._board[i][j].convertToCube() ;
    }

    // Sugar : Tracing the cube move
    trace(startPos, endPos, color){
        let start = this._board[startPos.row][startPos.column].getPosition();
        let end = this._board[endPos.row][endPos.column].getPosition();
        this._trace = new TraceObject(start.x, start.y, end.x, end.y, color == 1);
    }

    // Display function called from 
    // P5js draw function /main.js ( ~60times/sec )
    // Feeds the rendering pipeline
    // Contains winner = { team: team, line: line, col: col, diag: diag}
    //  = (CubeStates), int(0..4), int(0..4), int(1-2)
    show(winner) {
        fill(0)
        rect((width - this.boardSize) / 2,
            (height - this.boardSize) / 2,
            this.boardSize, this.boardSize);
        fill(255);
        
        if (winner && ! this._winner){
            this._winner = winner;
            this._explose(windowWidth/2, windowHeight/2, winner.team == -1 );
            //console.log(winner);
            if ( winner.line > -1 ){
                this._showLineWin(winner.line);
            } else if ( winner.col > -1){
                this._showColWin(winner.col);
            } else if ( winner.diag > -1 ){
                this._showDiagWin(winner.diag);
            }
        }
        else
            this._showDefault();


        /* Display effects */
        if ( this._trace ){
                this._trace.updater();
                this._trace.show();
        }
        for (let i = 0; i < this._explosion.length; i++){
            this._explosion[i].updater();
            this._explosion[i].show();
        }
    }

    // Display function called from 
    // P5js draw function /main.js ( ~60times/sec )
    // Feeds the rendering pipeline
    _showDefault() {
        for (let j = 0; j < 5; j++)
            for (let i = 0; i < 5; i++)
                this._board[i][j].show();
    }

    // Animate the winning column
    _showColWin(col){
        //console.log( "col n°"+col);
        for (let j = 0; j < 5; j++) {
            for (let i = 0; i < 5; i++) {
                this._convert( j === col, i, j );
                this._board[i][j].show();
            }
        }
    }

    // Animate the winning line
    _showLineWin(line){
        //console.log( "line n°"+line);
        for (let j = 0; j < 5; j++) {
            for (let i = 0; i < 5; i++) {
                this._convert( i === line, i, j )
                this._board[i][j].show();
                
            }
        }
    }

    // Animate the winning diagonal
    _showDiagWin(diag){
        //console.log( "diag n°"+diag);
        if ( diag == 1)
            for (let j = 0; j < 5; j++) {
                for (let i = 0; i < 5; i++) {
                    this._convert( i === j, i, j);
                    this._board[i][j].show();
                }
            }
        else
            for (let j = 0; j < 5; j++) {
                for (let i = 0; i < 5; i++) {
                    this._convert( i === 4 - j  && j === 4 - i, i, j);
                    this._board[i][j].show();
                }
            }

    }

    // Convert cube i, j
    // if(bool) => winner cube 
    // else => random animated looser cube 
    _convert(bool, i, j){
        if (bool)
            this._board[i][j] = this._board[i][j].convertToWinnerCube();
        else
            this._board[i][j] = this._board[i][j].convertToLooserCube(getRandomInt(4));
    }

    // Invoke Fireworks 
    _explose(mouseX,mouseY,color){
        if ( this._explosion.length < 100 )
            for (let i = 0; i < 40; i++){
                this._explosion.push(new Firework(mouseX, mouseY, color));
                this._nbExplo ++ ;
            }
        else{

            for (let i = 0; i < 40 ; i ++) {
                this._explosion[this._nbExplo] = new Firework(mouseX, mouseY, color);
                this._nbExplo  =  (this._nbExplo + 1) % 100;

            }
        }     
    }

}