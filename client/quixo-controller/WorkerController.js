// Simple WebWork controller
// Author RAYMONDAUD Quentin

// Manage some WebWorker
// Min-Max Agent for Quixo-Game

// Thread Pool Design Pattern

class WorkerController{

    _agentsWorker; // Instance of QuixoWorker
    _nbThread;
    _agentOutput;

    // Construct the Worker Pool
    constructor(){
        this._nbResolvedThread = 0;
    	if (window.Worker) {
            this._agentsWorker = [];
            this._nbThread = navigator.hardwareConcurrency; // C'est ici que votre navigateur déterminera sur combien de core il pourra tourner
            for ( let  i =  0 ; i < this._nbThread ; i ++ ) // Vous pouvez le fixer à 1 pour lancer tous les coups possible dans un seul thread
                this._agentsWorker.push(new Worker("../quixo-engine/quixo-workers/MinMaxWorker.js"));
        }
    }

    /* data = 
        {
            turn: turn,                 -1 || 1
            board: boardCpy,            [ 0, 1, -1 ,....]
            picks: this._validPicks,    List[{row,column}, .. ]
            moves: this._validMoves     List[{row,column}, .. ]
            deepth: _minMaxDeepth       int
        }  */
    // Determines from which possibility the thread will start the min-max
    // Schedule tasks to each thread
    // Each Thread will take a part of picks and compute minmax for each picks we allocate it
    minMaxCall(data, autoIncrease = false) {
        this._agentOutput = [];
        var self = this;
        let datas = [];
        for ( let i = 0 ; i < this._agentsWorker.length ; i ++ )
            datas.push( {   turn: data.turn,
                            board: data.board,
                            picks: [],
                            deepth: data.deepth
                        });

        for ( let j = 0 ; j < data.picks.length ; j++){
            datas[j % this._nbThread].picks.push(data.picks[j]);
        }

        // Uncomment this part if you want to automatically increase deepth
        // if each worker have less than 4 picks to play 
        if ( autoIncrease ){
            let prof = parseInt(4 / datas[this._nbThread-1].picks.length) -1 
            for ( let j = 0 ; j < this._nbThread ; j++){
                datas[j].deepth += prof;
            }
        }

        for ( let i = 0 ; i < this._agentsWorker.length ; i ++ ){ 
            this._agentsWorker[i].postMessage(datas[i]);  
            this._agentsWorker[i].onmessage = function(e) {
                self._pushWorkerBestMove(e.data);
            }
            
        }

    }

    // When a worker finished his simulation he just put his result in the array
    // If this is the last worker result : call applyBestMove
    _pushWorkerBestMove(data){
        this._agentOutput.push(data);
        //console.log(this._agentOutput.length, this._nbThread)
        if ( this._agentOutput.length === this._nbThread){
            this._applyBestMove(data.max)
        }
    }

    // Apply the best move of all workers moves
    _applyBestMove(max){
        let score = ( max ? -1000 : 1000 );
        let bestMove;
        for ( let threadResult of this._agentOutput ){
            //console.log(threadResult);
            if (max){
                if ( score <= threadResult.score){
                    score = threadResult.score;
                    bestMove = threadResult;
                }
            }
            else{
                if ( score >= threadResult.score ){
                    score = threadResult.score;
                    bestMove = threadResult;
                }
            }
        }
        // Determines the best mouve of

        // Call main controller functions
        //console.log(" CHOOSEN : " + bestMove );
        controller.selectSrc(bestMove.src.row, bestMove.src.column);
        controller.applyMove(bestMove.dest.row, bestMove.dest.column);
    }
}