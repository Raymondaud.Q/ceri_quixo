// Simple P5js app controller
// Author RAYMONDAUD Quentin



const APPStates = Object.freeze({
    "MENU": 1,
    "QUIXO": 2,
    "FINISHED": 3,
});

const GAMEModes = Object.freeze({
    "SOLO": 1,
    "VERSUS": 2,
    "FULLROBOT": 3,
    "SHARED": 4
});

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

// Mediator pattern & MVC main controller
class Controller { // Mediator, Controller for MVC Architecture, AND SUBJECT OF OBSERVER 

    // ####################################### Menu Controller  ##############################################
    _menuView;  // Start Menu View
    _gameMode;  // ENUMValue that determines if the controller needs ton invoke a WebWoker or no in order to :
                // Play against the user   : _gameMode = "SOLO"      set in startSolo function
                // Play agaînst itself     : _gameMode = "FULLROBOT" set in startRobot function
                // Human vs Human          : _gameMode = "VERSUS"    set in startVersus function ( No Worker )
    // #######################################################################################################

    // ######################### BOARD CONTROLLER ##########################
    _boardView;  // Game View
    __engine;     // Inner and private quixo game _engine
    _validPicks; // Seletables cubes as this._src | Seletected this._src
    _validMoves; // Available Moves with current src
    _hovered;    // Current Hoverred playable Cube
    _src;        // Selected Cube
    _result      // details of the current winner ( if defined )
    // #####################################################################

    _state;            // App State
    _workerController; // The instance of the WebWorker Manager in case of _gameMode == "SOLO" | "FULLROBOT"
    _minMaxDeepth;
    _netHandler; //  Web Socket Handler
    // Build the Quixo _Engine and Views
    constructor() {
        this._boardView = new Board();
        this._engine = new Quixo();
        this._validPicks = this._engine.getValidPicks();
        this.notifyBoardView();
        this._menuView = new Menu();
        this._state = APPStates.MENU;
    }

    setState(state){
        this._state = state;
    }

    getState() { return this._state; }

    isFinished(){
        let result  = this._engine.isFinished();
        if ( result )
            this._result = result;
    }

    // Not very well designed => Breaks encapsulation 
    // Only used for socket shared game Proof of concept
    setEngine(engine){ this._engine = engine; } 

    reinitCubes() { this._boardView.reinitCubes(); }

    setValidPicks(){
        this._validPicks = this._engine.getValidPicks();
        this._validMoves = [];
    }

    setValidMoves(){
        this._validMoves = this._engine.getValidMoves(this._src.row,this._src.column);
        this._validPicks = [];
        this._validPicks.push(this._src);
    }

    // Setup for Human vs Human :  Triggered through _menuView
    startVersus(){
        this._menuView.removeButtons();
        this._gameMode = GAMEModes.VERSUS;
        setTimeout(() => {  this._state = APPStates.QUIXO; }, 500);     
    }

    // Setup for shared game
    startShared(){
        this._menuView.removeButtons();
        this._gameMode = GAMEModes.SHARED;
        setTimeout(() => {  this._state = APPStates.QUIXO; }, 500);
        this._netHandler = new WebSocketHandler(this);  
    }


    // Setup for  Human vs Robot :  Triggered through _menuView
    startSolo(reverse, deepth){
        //console.log(reverse);
        this._minMaxDeepth = deepth;
        this._reverse = reverse;
        this._menuView.removeButtons();
        this._gameMode = GAMEModes.SOLO;
        this._workerController = new WorkerController();
        setTimeout(() => {  this._state = APPStates.QUIXO; }, 500);
        if ( this._reverse )
            this.notifyBoardView(true); // Uncomment for reverser start
    }

    // Setup for  Robot vs Robot :  Triggered through _menuView
    startRobot(deepth){
        this._minMaxDeepth = deepth;
        this._menuView.removeButtons();
        this._gameMode = GAMEModes.FULLROBOT;
        this._workerController = new WorkerController();
        setTimeout(() => {  this._state = APPStates.QUIXO; }, 500);
        this.notifyBoardView(true);
    }

    // Triggered on P5js mousePressed function
    // Handles gameplay when _state == QUIXO 
    select(mouseX, mouseY) {
        if ( this._state == APPStates.QUIXO){
            let tmpCube = this._boardView.getCubeIndex(mouseX, mouseY);

            // Select Current Cube - this._src 
            if (! this._src && tmpCube ){
                if (this._engine ){
                    const turn =  this._engine.getRound();
                    if ( this._gameMode === GAMEModes.SOLO && 
                       ( (this._reverse && turn === -1) || 
                       (! this._reverse && turn === 1) ) )
                        this.selectSrc(tmpCube.row, tmpCube.column);
                    else if ( this._gameMode !== GAMEModes.SOLO)
                        this.selectSrc(tmpCube.row, tmpCube.column);
                }
            }

            // Unselect Current Cube | console.log("Cube ["+this._src.column+" "+this._src.row+"] unselected" );
            else if ( tmpCube && this._src && objectsEqual(this._src, tmpCube) ){
                this._validPicks = this._engine.getValidPicks();
                this._src = undefined;
                this._validMoves = undefined;
                this.notifyBoardView();
            }

            // Apply a move
            else if (this._src && tmpCube)
                this.applyMove( tmpCube.row, tmpCube.column);

        }
        else if ( this._state === APPStates.MENU){
            this._menuView.explose(mouseX,mouseY);
        } else if ( this._state === APPStates.FINISHED){
            this.reinitCubes();
            this._result = undefined;
            this._engine = new Quixo();
            this._state = APPStates.QUIXO;
            this.setValidPicks();
            this.notifyBoardView(true);
            this._src = undefined;

        }
    }

    // Set this._src on cube {row, column} and notify update
    selectSrc(row, column){
        if ( this._validPicks.findIndex( element => 
                                            element.column === column &&
                                            element.row === row ) > -1){
            this._src = { column: column, row: row};
            this.setValidMoves();
            this.notifyBoardView();
        }
    }

    // Move this._src to {row, column} and notify update
    applyMove(row, column){
        if (this._validMoves){
            if ( this._validMoves.findIndex( element => 
                                            element.column === column &&
                                            element.row === row ) > -1 )
                if (this._engine.play(this._src.row, this._src.column, row, column)){
                    // ws.send => envoie tous les coups validés au server 
                    if ( this._gameMode == GAMEModes.SHARED )
                        this._netHandler.send("[" + JSON.stringify(this._src) +"," 
                                                        +  JSON.stringify({row:row, column:column}) + "]");

                    this._boardView.trace(this._src, {row: row, column: column} , -this._engine.getRound());
                    this._result = this._engine.isFinished()
                    if ( this._result )
                        this._state = APPStates.FINISHED;
                    this.setValidPicks();
                    this.notifyBoardView(true);
                    this._src = undefined;
                    return;
                }
        }
    }
  
    // Called from P5js mouseMoved Event in /main.js
    // Observable Subject Notify Function 
    // Triggered on mouse moved in order to detect BoardView elements hovering
    mouseMoved(mouseX,mouseY){
        if (this._hovered){ 
            this._hovered.setHovered(false);
            this._hovered = undefined;
        }
        let cubePos = this._boardView.getCubeIndex(mouseX, mouseY)
        if ( cubePos ){
            if ( this._validPicks.findIndex( element => 
                                    element.column === cubePos.column &&
                                    element.row === cubePos.row )  > -1 )
                this._hovered = this._boardView.getCube(mouseX, mouseY);
            
            if ( this._validMoves)
                if ( this._validMoves.findIndex( element => 
                                                element.column === cubePos.column &&
                                                element.row === cubePos.row ) > -1 )
                    this._hovered = this._boardView.getCube(mouseX, mouseY);
        }
        if (this._hovered)
            this._hovered.setHovered(true);
    }

    // Observer Subject Notify Function 
    // Only one observable component is displayed at time
    // So it doesn't require a list of observable
    notifyBoardView(newTurn = false ){
        const turn =  this._engine.getRound();
        const boardCpy = this._engine.getBoard();
        this._boardView.updateBoard(    boardCpy, 
                        this._validPicks,
                        this._validMoves,
                        turn);  // Subject Update function
        if ( newTurn && this._workerController != undefined && this._gameMode !== GAMEModes.VERSUS && this._state !== APPStates.FINISHED  && this._state !== APPStates.SHARED ){
            if  (( !this._reverse && ( turn === -1 || (turn === 1 && this._gameMode === GAMEModes.FULLROBOT ))) ||
                ( this._reverse && ( turn === 1 || (turn === -1 && this._gameMode === GAMEModes.FULLROBOT ))))
                this._workerController.minMaxCall({
                                                    turn: turn,
                                                    board: boardCpy,
                                                    picks: this._validPicks,
                                                    deepth: this._minMaxDeepth
                                                });
        }
    }

    // Render All P5js elements
    show(){
        if ( this._state === APPStates.QUIXO)
            this._boardView.show();
        if ( this._state === APPStates.MENU)
            this._menuView.show();
        if ( this._state === APPStates.FINISHED ){
            this._boardView.show(this._result);
            this._result = undefined;
        }
    }

    // Called from P5js mouseMoved Event in /main.js
    resize() {
        this._menuView.resize();
        this._boardView.resize();
        this.notifyBoardView();
    }


}