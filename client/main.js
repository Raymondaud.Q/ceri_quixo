// Quentin RAYMONDAUD
// FULL Standalone Quixo Game

var controller;
var IPORT = "localhost:3019";
// HTML5 Cavans Setup & binding to P5js rendering pipeline & DOM events
// Controller construction
function setup() {
  createCanvas(windowWidth, windowHeight);
  controller = new Controller();
}
// P5js Event Triggered on MouseClick ( left or right )
function mousePressed() {
  controller.select(mouseX, mouseY)
}

// P5js Event Triggered on MouseMove
function mouseMoved() {
  controller.mouseMoved(mouseX,mouseY)
}

// P5js Draw function, called ~60times/sec
function draw(){
  background(60);
  controller.show();
}

// P5js Event Triggered on WindowResized
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  controller.resize();  
} 
