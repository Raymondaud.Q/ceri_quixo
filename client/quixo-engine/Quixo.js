// RAYMONDAUD Quentin
// FULL JS naïve Quixo-Engine !


const BOARDSIZE = 5;

// Collection GLOBAL Function
const objectsEqual = (o1, o2) =>
  Object.keys(o1).length === Object.keys(o2).length &&
  Object.keys(o1).every(p => o1[p] === o2[p])

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

// ENUM STATES OF CUBES
const CubeStates = Object.freeze({
    "CROSS": -1,
    "CIRCLE": 1,
    "FREE": 0
});



class QuixoCube {
    _state; // Cube owned by (CubeStates)

    constructor(state) { 
        if ( ! arguments.length )
            this._state = this._state = CubeStates.FREE; 
        else 
            this._state = state;
    }

    setState(cs) { this._state = cs; }
    getState() { return this._state; }
    toString() {
        let cbStr = "";
        if (this._state === -1)
            cbStr += "X";
        else if (this._state === 1)
            cbStr += "O";
        else
            cbStr += '-';
        return "_" + cbStr + "_|";
    }
}

class Quixo {
    _round; // Current player round (CubeStates)
    _board; // 5*5 Board with QuixoCube
    _winner; // Undefined while not finished
    // Contains winner info as :
    // {team: team, line: line, col: col, diag: diag}
    //  = (CubeStates), int(0..4), int(0..4), int(1-2)

    constructor(board, turn) {
        if ( ! arguments.length ){
            this._round = CubeStates.CIRCLE;
            this._board = [];
            for (let i = 0; i < BOARDSIZE; i++) {
                this._board[i] = [];
                for (let j = 0; j < BOARDSIZE; j++)
                    this._board[i][j] = new QuixoCube()
            }
        } else {
            this._round = turn;
            this._board = []
            for (let i = 0; i < BOARDSIZE; i++) {
                this._board[i] = [];
                for (let j = 0; j < BOARDSIZE; j++)
                    this._board[i][j] = new QuixoCube(board[i][j]);
            }
        }
    }


    // Returns current team round
    getRound() { return this._round; }

    // Returns board state cpy
    getBoard() {
        var boardCpy = []
        for (let i = 0; i < BOARDSIZE; i++) {
            boardCpy[i] = [];
            for (let j = 0; j < BOARDSIZE; j++)
                boardCpy[i][j] = this._board[i][j].getState();
        }
        return boardCpy;
    }

    // Returns all selectable cubes by the current player
    getValidPicks() {
        if ( this._winner) return [];
        let validPicks = [];
        for (let i = 0; i < BOARDSIZE; i++) {
            for (let j = 0; j < BOARDSIZE; j++) {
                if (!( ( j !== BOARDSIZE - 1 &&
                        i !== BOARDSIZE - 1 &&
                        j !== 0 && i !== 0 ) ||
                        (this._board[i][j].getState() !== this._round &&
                        this._board[i][j].getState() !== CubeStates.FREE) ) )
                    validPicks.push({
                        column: j,
                        row: i
                    });
            }
        }
        return validPicks;
    }

    // Returns possible move with the cube at <y,x>
    getValidMoves(y, x) {
        if ( this._winner) return undefined;
        let actualPos = {
            column: x,
            row: y
        };
        let posList = [];
        if ((x !== BOARDSIZE - 1 && y !== BOARDSIZE - 1 &&
                x !== 0 && y !== 0) ||
            (this._board[y][x].getState() !== this._round &&
                this._board[y][x].getState() !== CubeStates.FREE))
            return undefined;
        else {
            if (x != 0)
                posList.push({column: 0, row: y});
            if (x != BOARDSIZE - 1)
                posList.push({column: BOARDSIZE - 1, row: y});
            if (y != 0)
                posList.push({column: x, row: 0});
            if (y != BOARDSIZE - 1)
                posList.push({column: x, row: BOARDSIZE - 1});
            return posList;
        }
        return undefined;
    }

    // Put a cube from src to dest & push the line
    play(ySrc, xSrc, yDest, xDest) {
        if ( this._winner) return false;
        let moves = this.getValidMoves(ySrc, xSrc);
        if (moves) {
            let dest = {
                column: xDest,
                row: yDest
            };
            let nbMove = moves.length; // OPTI : DON'T ACCESS ARRAY.length at each loop iteration
            for (let m = 0; m < nbMove; m++) {
                if (objectsEqual(moves[m], dest)) {
                    this._board[ySrc][xSrc].setState(this._round);
                    if (ySrc == yDest)
                        this._rollRow(ySrc, xSrc, xSrc > xDest);
                    else
                        this._rollColumn(ySrc, xSrc, ySrc > yDest)
                    this._round *= -1;
                    return true;
                }
            }
            return false;
        } else
            return false;
    }

    // Check if game is finished
    // If it is, returns and fill property this._winner 
    // With all data about the winning combination
    // {team: team, line: line, col: col, diag: diag}
    //  = (CubeStates), int(0..4), int(0..4), int(1-2),  
    isFinished() {
        if ( this._winner) return this._winner;
        let diagSum3, diagSum1;
        diagSum3 = diagSum1 = 0;
        let team, line, col, diag;
        line = col = diag = -1; team = -2;
        for (let i = 0; i < BOARDSIZE; i++) {
            let lineSum = this._board[i].reduce((a, b) => a + b.getState(), 0)
            let colSum = this._board.map(x => x[i]).reduce((a, b) => a + b.getState(), 0);
            team = line = col = diag =-2;
            if ( Math.abs(lineSum) === BOARDSIZE){
                team = lineSum > 0 ? CubeStates.CIRCLE : CubeStates.CROSS;
                line = i;
                this._winner = {team: team, line: line, col: col, diag: diag}
                return this._winner;
                
            } else if ( Math.abs(colSum) === BOARDSIZE){
                team = colSum > 0 ? CubeStates.CIRCLE : CubeStates.CROSS;
                col = i;
                this._winner = {team: team, line: line, col: col, diag: diag}
                return this._winner;
            }
            diagSum1 += this._board[i][i].getState();
            diagSum3 += this._board[BOARDSIZE-1 - i][i].getState();
        }
        if ( Math.abs(diagSum1) === BOARDSIZE){
            team = diagSum1 > 0 ? CubeStates.CIRCLE : CubeStates.CROSS;
            diag = 1;
            this._winner = {team: team, line: line, col: col, diag: diag}
            return this._winner;  
        } else if ( Math.abs(diagSum3) === BOARDSIZE){
            team = diagSum3 > 0 ? CubeStates.CIRCLE : CubeStates.CROSS;
            diag = 2;
            this._winner = {team: team, line: line, col: col, diag: diag}
            return this._winner;
        }
    }

    // Push a row
    _rollRow(ySrc, xSrc, way) {
        if (way) // decale src to the left
            for (let i = xSrc; i > 0; i--) {
                let tmp = this._board[ySrc][i];
                this._board[ySrc][i] = this._board[ySrc][i - 1];
                this._board[ySrc][i - 1] = tmp;
            }
        else // decale src to the right
            for (let i = xSrc; i < BOARDSIZE - 1; i++) {
                let tmp = this._board[ySrc][i];
                this._board[ySrc][i] = this._board[ySrc][i + 1];
                this._board[ySrc][i + 1] = tmp;
            }
    }

    // Push a column
    _rollColumn(ySrc, xSrc, way) {
        if (way) // decale src to the top
            for (let i = ySrc; i > 0; i--) {
                let tmp = this._board[i][xSrc];
                this._board[i][xSrc] = this._board[i - 1][xSrc];
                this._board[i - 1][xSrc] = tmp;
            }
        else // decale src to the right
            for (let i = ySrc; i < BOARDSIZE - 1; i++) {
                let tmp = this._board[i][xSrc];
                this._board[i][xSrc] = this._board[i + 1][xSrc];
                this._board[i + 1][xSrc] = tmp;
            }
    }

    toString() {
        let strQuixo = ""
        for (let i = 0; i < BOARDSIZE; i++) {
            if (i == 0) strQuixo += "y\\x|_0_|_1_|_2_|_3_|_4_|";
            strQuixo += '\n' + str(i) + "->|";
            for (let j = 0; j < BOARDSIZE; j++)
                strQuixo += this._board[i][j].toString();
        }
        return strQuixo;
    }
} // End Class