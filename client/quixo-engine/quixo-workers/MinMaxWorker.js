// WebWorker Quixo Agent
// Author RAYMONDAUD Quentin

// This script is launched in his own JS runtime through /quixo-controller/WorkerController.js
// Thats how we multithread in JS


// Interface with main Thread

    /* IN : e.data = 
        {
            turn: turn,                 -1 || 1
            board: boardCpy,            [ 0, 1, -1 ,....]
            picks: this._validPicks,    List[{row,column}, .. ]
            moves: this._validMoves     List[{row,column}, .. ]
        } 

        OUT :  bestMove = {
                src:  {row, column},
                dest: {row, column} 
            }

    */   

class MinMax{
    _max;
    _team

    constructor(){}

    // MinMax function
    // Can't do it in two function ( min() & max() ) because JS interperter isn't smart enough
    // It block deep recursion  
    minMax( board, turn, picks, profondeur, max, call=false){

        let fork = new Quixo(board,turn);

        if ( profondeur === 0)
            return {score: this.eval(fork), bestMove: undefined};
        
        if ( call ){
            this._team = turn;
            this._max =  max
        }

        let score = ( max  ? -100 : 100 );
        let bestMove;
        let moves;
        for ( let pick of picks){
            moves = fork.getValidMoves(pick.row, pick.column);
            for ( let move of moves ){
                fork.play(pick.row, pick.column, move.row, move.column);
                let winner = fork.isFinished();
                if ( winner &&  call ){
                    if( this._team === winner.team ){ // If win in a direct moove
                        bestMove = { src: pick, dest: move} ;
                        score =  ( this._max ? 100 : -100 );
                        return ( { bestMove: bestMove, score: score})
                    } else 
                       score = ( this._max ?  -100 : 100 ); 
                } 
                else if ( winner ){
                    if( turn === winner.team ){
                        if ( ( max ? 100 > score : -100 < score ) ){
                            bestMove = { src: pick, dest: move} ;
                            score = ( max ? 100 : -100 );
                        }
                    }
                    else{
                        if ( ( max ? -100 > score : 100 < score ) )
                            score = ( max ? -100 : 100 );                 
                    }
                } else {
                    let deeper = this.minMax(fork.getBoard(), fork.getRound(), fork.getValidPicks(), profondeur-1, !max );
                    if ( max ) {
                        if ( deeper.score >= score ){
                            score = deeper.score
                            bestMove = { src: pick, dest: move};
                        }
                    } else {
                        if ( deeper.score <= score ) {
                            score = deeper.score
                            bestMove = { src:pick, dest: move};
                        }
                    }
                }
                fork = new Quixo(board,turn);
            }
        }
        return {score: score, bestMove: bestMove};
    }

    // Naive eval function
    eval(fork){
         let winner = fork.isFinished();
        if ( winner ){
            if ( winner.team == this.team)
                return  this._max ? 100 : -100 ;
            else
                return this._max ?  -100 : 100 ;
        }
        let sum = 0;
        let board = fork.getBoard();
        for (let i = 0; i < BOARDSIZE; i++) {
            let lineSum = board[i].reduce((a, b) => a + b, 0)
            let colSum = board.map(x => x[i]).reduce((a, b) => a + b, 0);
            sum += colSum 
            sum += board[i][i]
            sum += board[BOARDSIZE-1 - i][i]
        }   
        for (let i = 0; i < BOARDSIZE; i++) 
            for ( let j = 0; j < BOARDSIZE; j++ )
                sum += board[i][j];     
        
        return ( this._max ? sum * this._team : -sum * this._team ); 
    }
}

// Worker Trigger function
onmessage = function(e) {
    if (typeof Quixo !== 'function')
        importScripts('../Quixo.js');  // Import the engine 
    //console.log(e.data.picks);
    const result = e.data;  
    if (!result || result.picks.length == 0)
        postMessage('No Data provided');
    else {
        let minMax = new MinMax();
        let prof = result.deepth;
        // console.log(prof);
        let max = (prof%2 === 0);
        let mmResult = minMax.minMax(result.board, result.turn, result.picks, prof , max, true);
        postMessage( { 
                        score: mmResult.score,
                        src: mmResult.bestMove.src, 
                        dest: mmResult.bestMove.dest,
                        max : max
                    }); 
    }
 
}

