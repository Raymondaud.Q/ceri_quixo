// WebWorker Quixo Agent
// Author RAYMONDAUD Quentin

// This script is launched in his own JS runtime through /quixo-controller/WorkerController.js
// Thats how we multithread in JS

// THIS IS A TEMPLATE FOR BUILDING YOUR OWN AGENT 
// MY MINMAX AGENT IS IN ./MinMaxWorker.js

// Interface with main Thread

    /* IN : e.data = 
        {
            turn: turn,                 -1 || 1
            board: boardCpy,            [ 0, 1, -1 ,....]
            picks: this._validPicks,    List[{row,column}, .. ]
            moves: this._validMoves     List[{row,column}, .. ]
        } 

        OUT :  bestMove = {
        		src:  {row, column},
        		dest: {row, column} 
        	}

    */   
onmessage = function(e) {
	if (typeof Quixo !== 'function')
		importScripts('../Quixo.js');   							// Import the engine 
	console.log('Start MinMax:');
	const result = e.data;
	if (!result)
		postMessage('No Data provided');
	else {
		let quixoFork = new Quixo(result.board, result.turn);		// Fork the engine 

		postMessage( { 
						src: result.picks[0], 
						dest: quixoFork.getValidMoves(result.picks[0].row, result.picks[0].column )[0] 
					}); 
	}
 
}
