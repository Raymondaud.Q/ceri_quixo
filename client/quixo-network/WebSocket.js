// Quentin RAYMONDAUD
// Minimal WebSocket Support for shared game

class WebSocketHandler{
    constructor(controller){
        this.ws = new WebSocket('ws://'+IPORT+'/');
        this.ws.onmessage = function (event) { // Handle received data from server
            console.log(event.data);
            if ( isJson(event.data)){
                let data = JSON.parse(event.data);
                if ( data.length == 2 ) {
                    if ( data[0].length == 5 ){                                 // Array [Board, turn] Received
                        controller.setEngine(new Quixo(data[0], data[1]));      // Not opti at all but it's a Working Proof of Concept
                        controller.isFinished();

                        if ( controller.getState() === APPStates.FINISHED ){
                            controller.setState(APPStates.QUIXO);
                            controller.setEngine(new Quixo(data[0], data[1]));
                            controller.reinitCubes();
                            controller.notifyBoardView();
                        }

                        if ( controller._result )
                            controller.setState(APPStates.FINISHED);

                        controller.setValidPicks();
                        controller.notifyBoardView();
                    }
                }
            }
        };
    }

    send(data){
        this.ws.send(data);
    }
}